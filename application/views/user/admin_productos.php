<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
#g-table tbody tr > td{
    border: 1px solid rgb(220,220,220);
    height: 30px;
    padding-left: 3px;
}
#g-table{
    padding-left: 40px;
    margin-top: 20px;

}
nav > ul {
  display: flex;
  flex-direction: column;
  align-items: center;
}
body{
    font-family: Arial, Helvetica, sans-serif;
}

form{
    background-color: black;
    margin: 0 auto;
    width: 400px;
    padding: 20px;
}

input{
    border: solid 0;
    border-radius: 3px;
}

input[type=text], input[type=password]{
    padding: 10px;
    font-size: 18px;
    outline: none;
    width: 370px;
}
input[type=submit]{
    background-color: #1E69E3;
    color: white;
    padding: 8px;
    border: none;
    width: 200px;
}
.center{
    text-align: center;
}

.opcion{
    padding: 5px 0;
}

.barra{
    background-color:rgb(152, 196, 236);
    border-radius: 4px;
    padding: 10px;
}

.seleccionado{
    background-color: rgb(33, 90, 143);
    border-radius: 4px;
    color: white;
    padding: 10px;
}

#menu{
    background-color: gray;
    padding: 10px;
}
#menu ul{
    margin: 0;
    padding: 0;
    list-style: none;
    display: inline-block;
    width: 100%;
}
#menu ul li{
    display: inline;
}
#menu ul li a{
    color: #1E69E3;
    text-decoration: none;
}
#menu ul li a:hover{
    color: rgb(227, 109, 30);
    text-decoration: none;
}
.cerrar-sesion{
    float: right;
}
</style>
<body style="background-color:gray">
    <div id="menu">
        <ul>
            <li>Home - Administrador</li>
            <li class="cerrar-sesion"><a href="includes/logout.php">Cerrar sesión</a></li>
        </ul>
    </div>
    <table align="center" class="table table-light"  id="g-table">
      <tr>
        <th>SKU</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Imagen</th>
        <th>Categoria</th>
        <th>Stock</th>
        <th>Precio</th>
      </tr>
      <tbody>
        <?php
          $page = 'admin/admin_productos.php';  
            foreach ($productos as $producto) {
                if($producto->imagen == null){
                  $imagen="producto.png";
                  $producto->imagen = $imagen;
                }
                echo "<tr><td>".$producto->sku."</td><td>".$producto->nombre."</td><td>".$producto->descripcion."</td><td><img src='../../images/$producto->imagen'  width='100px' class='img-thumbnail'></td><td>".$producto->categoria."</td><td>".$producto->stock."</td><td>".$producto->precio."</td><td><a href='/user/edit_producto?id=".$producto->sku."'><input type='button' value='Editar 📝'></a><td><a href='/user/delete_productos?id=".$producto->sku."'><input type='button' value='Eliminar 🗑'></td></a></tr>";              
            }             
        ?>
      </tbody>
    </table>
    <div style="text-align: center;">
      <a href="/user/home_admin"><input type="button" value="Home - Admin"></a>
      <a href="/user/admin_nuevo_producto"><input type="button" value="Nuevo producto"></a>
    </div>
</body>
</html>