<?php
if (isset($_GET["id"])) {
  foreach ($user as $us) {
    $id = $us->id;
    $nombre = $us->name;
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home</title>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
  }

  li {
    float: left;
    border-right: 1px solid #bbb;
  }

  li:last-child {
    border-right: none;
  }

  li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
  }

  li a:hover:not(.active) {
    background-color: #111;
  }

  li input {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    background-color: purple;
  }

  li input:hover:not(.active) {
    background-color: #111;
  }

  .active {
    background-color: purple;
  }

  #g-table tbody tr>td {

    height: 30px;
    padding-left: 3px;
  }

  #g-table {
    padding-left: 40px;
    margin-top: 20px;
  }

  nav>ul {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .enlace_desactivado {
    pointer-events: none;
    cursor: default;
  }
</style>

<body style="background-color:gray">
  <form action="" method="POST" ectype="multipart/form-data">
    <div id="menu">
      <ul>
        <li style="float:right"><a href="/user/logout">Cerrar sesión</a></li>
        <li style='float:right'><a href='/user/home_cliente?id=<?php echo $id?>&id_cat=<?php echo $_GET['id_ca']?>'>Volver</a></li>
        <?php
        foreach ($num as $t) {
          $total = $t->total;
          if ($total == null) {
            $total = 0;
          }
        }
        echo "<li style='float:left' class='active'><a href='/user/carrito?id=$id'>🛒 ($total)</a></li>";
        ?>
      </ul>
    </div>
    <table align="center" class="table table-light" id="g-table">
      <tbody>
        <?php
        $cantd = 0;
        foreach ($cantidad as $cant) {
          $cantd = $cantidad->cantidad;
        }
        $monto_total = 0;
        foreach ($productos as $producto) {
          $page = '/user/carrito';
          echo "<center><h4>SKU: $producto->sku /$producto->nombre $producto->descripcion</h4><h4>₡$producto->precio</h4><img src='../../images/$producto->imagen.' width='400px' class='img-thumbnail' height='400px'></center><br>";
          $stock = $producto->stock;
          if ($cantd >= $stock) {
            echo "<center><a href='/user/addCarrito?cant=1&id_p=".$producto->sku."&id=$id'><input type='button' value='Añadir 🛒' disabled='true'></a></center>";
          } else {
            echo "<center><a href='/user/addCarrito?cant=1&id_p=".$producto->sku."&id=$id'><input type='button' value='Añadir 🛒'></a></center>";
          }
        }
        ?>
  </form>
  </tbody>
  </table>
</body>

</html>