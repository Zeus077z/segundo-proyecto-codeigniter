<?php
  foreach ($user as $us){
    $id = $us->id;
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home</title>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
  }

  li {
    float: left;
    border-right: 1px solid #bbb;
  }

  li:last-child {
    border-right: none;
  }

  li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
  }

  li a:hover:not(.active) {
    background-color: #111;
  }

  li input {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    background-color: purple;
  }

  li input:hover:not(.active) {
    background-color: #111;
  }

  .active {
    background-color: purple;
  }

  #g-table tbody tr>td {
    border: 1px solid rgb(220, 220, 220);
    height: 30px;
    padding-left: 3px;
  }

  #g-table {
    padding-left: 40px;
    margin-top: 20px;
  }

  nav>ul {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
</style>
<body style="background-color:gray">
  <form action="" method="POST" ectype="multipart/form-data">
    <div id="menu">
      <ul>      
        <li style="float:right"><a href="user/logout">Cerrar sesión</a></li>
        <li style="float:right" class="active"><a href='/user/home_cliente?id=<?php echo "$id" ?>'>Volver</a></li>
      </ul>
    </div>
    <table align="center" class="table table-light" id="g-table">
      <tr>
        <th>Fecha de compra</th>
        <th>Total de la orden</th>
        <th>Item</th>
        <th>Cantidad</th>
        <th>Descripcion</th>
        <th>Precio</th>
      </tr>
      <tbody>
        <?php
        foreach ($compras as $compra) {
          $fecha_compra = $compra->fecha_compra;
          $total_orden = $compra->total_orden;
          $item = $compra->item;
          $cantidad = $compra->cantidad;
          $descripcion = $compra->descripcion;
          $precio = $compra->precio;
          echo "<tr><td>$fecha_compra</td><td>$total_orden</td><td>$item</td><td>$cantidad</td><td>$descripcion</td><td>$precio</td></tr>";
        }
        foreach ($cantidad_productos as $cantidad_producto) {
          if ($cantidad_producto->total == null) {
            $cantidad_products = 0;
          } else {
            $cantidad_products = $cantidad_producto->total;
          }
        }
        foreach ($total_orden as $ventas) {
          if ($ventas->total == null) {
            $total_ord = 0;
          } else {
            $total_ord = $ventas->total;
          }
        }
        echo "<table align='center' class='table table-light'  id='g-table'>
                <tbody>
                    <tr>
                    <td><strong>Productos totales: $cantidad_products</strong></td>
                    <td><strong>Monto total: ₡$total_ord</strong></td></tr>
                </tbody>
              </table>";
        ?>
  </form>
  </tbody>
  </table>
</body>

</html>