<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

  /**
   * Shows the signup page
   *
   */
  public function signup()
  {
    $this->load->view('user/signup');
  }

  public function carrito()
  {
    $data['user'] = $this->User_model->LoadUser($_GET["id"]);
    $data['productos'] = $this->User_model->LoadCarrito($_GET["id"]);
    $this->load->view('user/carrito', $data);
  }

  public function admin_categorias()
  {
    $this->load->view('user/admin_categorias');
  }
  public function vista(){
    $data['cantidad'] = $this->User_model->cantidadCarrito($_GET["id_ca"], $_GET["id"]);
    $data['num'] = $this->User_model->CarritoNum($_GET["id"]);
    $data['user'] = $this->User_model->LoadUser($_GET["id"]);
    $data['productos'] = $this->User_model->LoadCarrito($_GET["id"]);
    $this->load->view('user/vista',$data);
  }
  public function ver_compras()
  {
    $data['total_orden'] = $this->User_model->totalOrden($_GET["id"]);
    $data['cantidad_productos'] = $this->User_model->cantidadProductos($_GET["id"]);
    $data['user'] = $this->User_model->LoadUser($_GET["id"]);
    $data['compras'] = $this->User_model->compras($_GET["id"]);
    $this->load->view('user/ver_compras', $data);
  }

  public function admin_nuevo_producto()
  {
    $data['categorias'] = $this->User_model->allCategorias();
    $this->load->view('user/admin_nuevo_producto', $data);
  }

  public function admin_nueva_categoria()
  {
    $this->load->view('user/admin_nueva_categoria');
  }

  public function admin_productos()
  {
    $data['productos'] = $this->User_model->allProductos();
    $this->load->view('user/admin_productos', $data);
  }

  public function logout()
  {
    session_unset();
    session_destroy();
    $this->load->view('user/login');
  }

  public function edit()
  {
    $data['users'] = $this->User_model->LoadUser($_GET["id"]);
    $this->load->view('user/edit', $data);
  }
  public function edit_categoria()
  {
    $data['categorias'] = $this->User_model->LoadCategorias($_GET["id"]);
    $this->load->view('user/edit_categoria', $data);
  }

  public function edit_producto()
  {
    $data['categorias'] = $this->User_model->allCategorias();
    $data['productos'] = $this->User_model->LoadProductos($_GET["id"]);
    $this->load->view('user/edit_producto', $data);
  }

  public function home_admin()
  {
    $data['clientes_registrados'] = $this->User_model->clientesRegistrados();
    $data['productos_vendidos'] = $this->User_model->productosVendidos();
    $this->load->view('user/home_admin', $data);
  }
  public function update_carrito()
  {
    $this->User_model->updateCarrito($_GET["id_p"], $_GET['cant'], $_GET['id']);
    redirect(site_url(['user', 'carrito']));
  }
  public function addCarrito()
  {
    $id_p = $_GET['id_p'];
    $cant = $_GET['cant'];
    $id_usuario = $_GET['id'];
    $result = $this->User_model->insertCarrito($id_p, $cant, $id_usuario);
    $data['user'] = $this->User_model->LoadUser($_GET["id"]);
    $this->load->view('user/home_cliente',$data);
  }
  public function home_cliente()
  {
    $data['total_orden'] = $this->User_model->totalOrden($_GET["id"]);
    $data['cantidad_productos'] = $this->User_model->cantidadProductos($_GET["id"]);
    $data['num'] = $this->User_model->CarritoNum($_GET["id"]);
    $data['categorias'] = $this->User_model->allCategorias();
    if(isset($_GET["id_cat"])){
      $data['cantidad'] = $this->User_model->cantidadCarrito($_GET["id_cat"], $_GET["id"]);
      $data['productos'] = $this->User_model->cargarProductosCliente($_GET["id_cat"]);
    }
    $data['user'] = $this->User_model->LoadUser($_GET["id"]);
    $this->load->view('user/home_cliente', $data);
  }
  public function cargarProductosCliente()
  {
    $data['total_orden'] = $this->User_model->totalOrden($_GET["id"]);
    $data['cantidad_productos'] = $this->User_model->cantidadProductos($_GET["id"]);
    $data['num'] = $this->User_model->CarritoNum($_GET["id"]);
    $data['categorias'] = $this->User_model->allCategorias();
    $data['user'] = $this->User_model->LoadUser($_GET["id"]);
    $data['productos'] = $this->User_model->cargarProductosCliente($_GET["id_cat"]);
    $data['cantidad'] = $this->User_model->cantidadCarrito($_GET["id_cat"], $_GET["id"]);
    $this->load->view('user/home_cliente', $data);
  }

  public function edit_categorias()
  {
    $id = $this->input->post('txtId');
    $name = $this->input->post('txtCategoria');
    $this->User_model->update_categoria($id, $name);
    redirect(site_url(['user', 'admin_categorias']));
  }

  public function edit_productos()
  {
    $id = $this->input->post('txtId');
    $name = $this->input->post('txtNombre');
    $description = $this->input->post('txtDescripcion');
    $img = $this->input->post('txtImg');

    $nombreArchivo = ($img != "") ? $img : "producto.png";
    $tmpImagen = $img;
    if ($tmpImagen != "") {
      move_uploaded_file($tmpImagen, "images/" . $nombreArchivo);
    }
    $category = $this->input->post('txtCategoria');
    $stock = $this->input->post('txtStock');
    $precio = $this->input->post('txtPrecio');
    $this->User_model->update_producto($id, $name, $description, $nombreArchivo, $category, $stock, $precio);
    redirect(site_url(['user', 'admin_productos']));
  }

  public function delete_productos()
  {
    $result = $this->User_model->delete_producto($_GET["id"]);
    if ($result) {
      $this->session->set_flashdata('msg', 'Product successfully removed');
      redirect(site_url(['user', 'admin_productos']));
    } else {
      redirect(site_url(['user', 'admin_productos']));
    }
  }

  public function delete_categorias()
  {
    $result = $this->User_model->delete_categoria($_GET["id"]);
    if ($result) {
      $this->session->set_flashdata('msg', 'User successfully removed');
      redirect(site_url(['user', 'admin_categorias']));
    } else {
      $this->session->set_flashdata('msg', 'There are products associated with this category');
      redirect(site_url(['user', 'admin_categorias']));
    }
  }

  public function delete_carrito()
  {
    $result = $this->User_model->delete_carrito($_GET["id_p"]);
    if ($result) {
      $this->session->set_flashdata('msg', 'Item successfully removed');    
    }
    $data['productos'] = $this->User_model->LoadCarrito($_GET["id"]);
    $data['user'] = $this->User_model->LoadUser($_GET["id"]);
    $this->load->view('user/carrito', $data);
  }

  public function login()
  {
    $username = $this->input->get('u');
    $password = $this->input->get('p');
    $data['username'] = $username;
    $data['password'] = $password;
    $this->load->view('user/login', $data);
  }
  /**
   * List existing users
   */
  public function list()
  {
    $data['users'] = $this->User_model->all();
    $this->load->view('user/list', $data);
  }
  public function authenticate()
  {
    // read login params (user/pass)
    $username = $this->input->post('username');
    $pass = $this->input->post('pass');
    $valid = $this->User_model->authenticate($username, $pass, 1);
    if ($valid) {
      $data['user'] = $this->User_model->authenticate($username, $pass, 1);
      $data['clientes_registrados'] = $this->User_model->clientesRegistrados();
      $data['productos_vendidos'] = $this->User_model->productosVendidos();
      $data['total_ventas'] = $this->User_model->totalVentas();
      $this->load->view('user/home_admin', $data);
    } else {
      $data['total_orden'] = $this->User_model->totalOrden(7);
      $data['cantidad_productos'] = $this->User_model->cantidadProductos(7);
      $data['num'] = $this->User_model->CarritoNum(7);
      $data['categorias'] = $this->User_model->allCategorias();
      $data['user'] = $this->User_model->authenticate($username, $pass, 2);
      $this->load->view('user/home_cliente', $data);
    }
  }
  /**
   * Creates a new user
   */
  public function create()
  {
    // input validations (password lenght, etc)
    $result = $this->User_model->insert($this->input->post());
    if ($result) {
      $this->session->set_flashdata('msg', 'User created, please login');
      redirect(site_url(['user', 'login']));
    } else {
      // send errors
      redirect(site_url(['user', 'signup']));
    }
  }

  public function new_categoria()
  {
    // input validations (password lenght, etc)
    $result = $this->User_model->insertCategoria($this->input->post());
    if ($result) {
      redirect(site_url(['user', 'admin_categorias']));
    } else {
      // send errors
      redirect(site_url(['user', 'admin_nueva_categoria']));
    }
  }
  public function new_producto()
  {
    // input validations (password lenght, etc)
    $result = $this->User_model->insertProducto($this->input->post());
    if ($result) {
      redirect(site_url(['user', 'admin_productos']));
    } else {
      // send errors
      redirect(site_url(['user', 'admin_nuevo_producto']));
    }
  }
  public function delete()
  {
    $this->User_model->delete($_GET["id"]);
    redirect(site_url(['user', 'list']));
  }
}
